import Data.Time.Clock
import Data.Time.Calendar
import Data.Time.Calendar.WeekDate
import Data.Time.Format

import Graphics.Rendering.Cairo
import Control.Monad

import qualified Data.Map as M

data Color = Gray         |
             White        |
             Red          |
             Green        |
             Blue         |
             Cyan         |
             Yellow       |
             Magenta      |
             RGB Double Double Double |
             Dark Color   |
             Light Color deriving(Show)

type RgbColor = (Double, Double, Double)

darken :: RgbColor -> RgbColor
darken (r,g,b) = let f = 0.9 in (f*r,f*g,f*b)

lighten :: RgbColor -> RgbColor
lighten = invert.darken.invert

invert :: RgbColor -> RgbColor
invert (r,g,b) = (1-r, 1-g, 1-b)

toRgb :: Color -> RgbColor
toRgb Gray    = (0.8,0.8,0.8)
toRgb White   = (1.0,1.0,1.0)
toRgb Red     = (1.0,0.0,0.0)
toRgb Green   = (0.0,1.0,0.0)
toRgb Blue    = (0.0,0.0,1.0)
toRgb Yellow  = (1.0,1.0,0.0)
toRgb Cyan    = (0.0,1.0,1.0)
toRgb Magenta = (1.0,0.0,1.0)

toRgb (RGB rr gg bb) = (rr, gg, bb)

toRgb (Dark color) = let f = 0.9
                         (r,g,b) = toRgb color
                     in (f*r,f*g,f*b)
toRgb (Light color) = let f = 0.9
                          (r,g,b) = toRgb color
                      in (1-f*(1-r),1-f*(1-g),1-f*(1-b))


chunks _ [] = []
chunks n l  = (take n l):(chunks n (drop n l))

dayOfWeek date = d where
    (_,_,d) = toWeekDate date

previousMonday date = addDays (-(fromIntegral (dayOfWeek date - 1))) date where

nextSunday date = addDays 6 (previousMonday date)

getCurrentDate = getCurrentTime >>= return . utctDay

showFull date = formatTime defaultTimeLocale format date where
    (_,m,d) = toGregorian date
    format = "%e %b '%y"

prettify date = formatTime defaultTimeLocale format date where
    (_,m,d) = toGregorian date
    format = if (d==1) then
                 if (m==1) then
                     "%e %b '%y"
                 else
                     "%e %b"
             else
                 "%e"

data WeekendMode = WithWeekends | WithoutWeekends

calendar :: Day -> Color -> Color -> WeekendMode -> [[(String, Color)]]
calendar firstDay oddMonthColor evenMonthColor wm = chunks nDays $ zip labels colors where
    nDays = case wm of WithWeekends    -> 7
                       WithoutWeekends -> 5
    labels = (showFull (head dates)):(map prettify (tail dates))
    colors = map dateColor dates
    startingDate = previousMonday firstDay
    dates = case wm of WithWeekends    -> allDates
                       WithoutWeekends -> filter (not.isWeekend) allDates
            where
                allDates = enumFrom startingDate
                isWeekend = ( >= 6 ) . dayOfWeek
    dateColor date = color where
        (_,month,_) = toGregorian date
        weekday = dayOfWeek date
        baseColor = if (month `mod` 2 == 0) then evenMonthColor else oddMonthColor
        color = if weekday <=5 then baseColor else (Dark baseColor)

mm = (*) (72.0/25.4)

makePicture draw file = withPDFSurface file (mm 210) (mm 297)
    (\s -> renderWith s $ (draw >> showPage))

rectangleWithText (w,h) (x,y) text color@(rr,gg,bb) = do
    rectangle (mm x) (mm y) (mm w) (mm h)
    setSourceRGB rr gg bb
    fillPreserve
    setSourceRGB 0 0 0
    setLineWidth (mm 1)
    stroke

    let fs = 12
    setFontSize fs
    let e = textExtents text
    labelWidth <- fmap textExtentsXadvance e
    labelHeight <- fmap textExtentsHeight e
    moveTo ((mm (x+0.5*w))-0.5*labelWidth) ((mm y)+fs*1.5)
    showText text

makeGrid :: (Double, Double) -> (Double, Double) -> [[(String, Color)]] -> [Render ()]
makeGrid (cellX, cellY) (pageX, pageY) rows = let
    gridX = length $ head rows
    gridY = length rows
    paddingX  = 0.5*(pageX-(fromIntegral gridX)*cellX)
    paddingY  = 0.5*(pageY-(fromIntegral gridY)*cellY)
    coords =  [(paddingX + fromIntegral i*cellX,
                paddingY + fromIntegral j*cellY) | j <- [0..(gridY -1)], i<-[0..(gridX -1)]]
    items = concat rows
    labels = map fst items
    colors = map (toRgb.snd) items
    in zipWith3 (rectangleWithText (cellX, cellY)) coords labels colors

main = do
    today <- getCurrentDate
--     let date = addDays 7 today
    let date = today
    let color1 = RGB (191.0/255.0) (144.0/255.0) (207.0/255.0)
    let color2 = RGB (156.0/255.0) (201.0/255.0) (140.0/255.0)
    let draw = sequence_ $ makeGrid (25, 25) (210, 297) $ take 11 $ calendar date color1 color2 WithWeekends
    makePicture draw "weeks.pdf"
